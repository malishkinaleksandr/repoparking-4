﻿using Parking.Veihcle;
using System;

namespace Parking
{
    class Program
    {
        static void Main(string[] args)
        {
            Parking parking = new Parking(50);

            VehicleCollection vehicleCollection = new VehicleCollection()
        {
            new Car("Opel","AX6766AT"),
            new Car("Mazda","AX6523PP"),
            new Car("Lada","AX8951MT"),
        };

            vehicleCollection.Add(new Car("BMW", "AH7777HA"));

            foreach (BaseVehicle car in vehicleCollection)
            {
                parking.SetVehicleOnFreeSpace(car);
            }

            foreach (var car in vehicleCollection)
            {
                Console.WriteLine(car);
            }

            Console.WriteLine("На парковке");

            foreach (var space in parking.AllBusySpace())
            {
                Console.WriteLine($"Место № {space.ID} занято, там\n"+ space.Vehicle.ToString());
            }

            Console.ReadKey();
        }
    }
}
