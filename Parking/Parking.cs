﻿using Parking.ParkingSpace;
using Parking.Veihcle;
using System.Collections;
using System.Collections.Generic;

namespace Parking
{
    public class Parking : IEnumerable<BaseParkingSpase>
    {
        private ParkingSpaceElement[] _allParkingSpace;

        public BaseParkingSpase this[int index]
        {
            get { return _allParkingSpace[index]; }
        }

        public Parking(int maxParkingPlace)
        {
            _allParkingSpace = new ParkingSpaceElement[maxParkingPlace];

            for (int i = 0; i < _allParkingSpace.Length; i++)
            {
                _allParkingSpace[i] = new ParkingSpaceElement(i + 1);
            }
        }

        public bool SetVehicle(int spaceNumber, BaseVehicle vehicle)
        {
            return _allParkingSpace[spaceNumber].Add(vehicle);
        }

        public void SetVehicleOnFreeSpace(BaseVehicle vehicle)
        {
            foreach (var space in _allParkingSpace)
            {
                if (space.IsFree)
                {
                    space.Add(vehicle);
                    break;
                }
            }
        }

        public bool SubtractVehicle(int spaceNumber)
        {
            return _allParkingSpace[spaceNumber].Subtract();
        }

        public void ReplaceVehicle(int spaceNumber, BaseVehicle vehicle)
        {
            _allParkingSpace[spaceNumber].Replace(vehicle);
        }

        public IEnumerable<BaseParkingSpase> AllBusySpace()
        {
            foreach (var space in _allParkingSpace)
            {
                if (space.IsFree == false)
                {
                    yield return space;
                }                
            }
        }

        public IEnumerator<BaseParkingSpase> GetEnumerator()
        {
            return GetEnumerator();
            //foreach (var space in _allParkingSpace)
            //{
            //    yield return space;
            //}
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
