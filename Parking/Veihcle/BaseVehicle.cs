﻿namespace Parking.Veihcle
{
    public abstract class BaseVehicle
    {
        public string Model { get; protected set; }
        public string Number { get; protected set; }

        public override string ToString()
        {
            return $"Модель - {Model}\nНомер - {Number}";
        }
    }
}
